# v_vehicle

A simple finite state machine based on **github.com/looplab/fsm** that represents a vehicle

Running

```console
$ go run main.go
```

Running the tests

```console
$ go test test/* -v
```

Usage

```Go
	v := v_vehicle.New(
		123456,
		&v_vehicle.Coordinates{Longitude: 59.332335, Latitude: 18.064159},
		&v_user.UserPriviledges{
			v_user.EndUser: []string{
				v_vehicle.EventStartJourney,
				v_vehicle.EventEndJourney,
			},
			v_user.Hunter: []string{
				v_vehicle.EventCollect,
				v_vehicle.EventDrop,
				v_vehicle.EventDeliver,
			},
		},
    )
    
    someUser := &v_user.User{UserID: 999, UserLevel: v_user.EndUser}
    hunter := &v_user.User{UserID: 1000, UserLevel: v_user.Hunter}
    admin := &v_user.User{UserID: 2000, UserLevel: v_user.Admin}
    
    v.StartJourney(someUser)
    v.EndJourney(someUser)
    v.LowBattery()

    timer := time.NewTimer(time.Second)
	<-timer.C
    v.Collect(hunter)
    v.Drop(hunter)
    v.Deliver(hunter, &v_vehicle.Coordinates{Longitude: 12.345, Latitude: 56.789})
    v.ServiceMode(admin)
    v.Terminate(admin)
```

Output:
```
Vehicle 123456 is entering state "riding"
User 999 started journey on vehicle 123456
User 999 ended journey on vehicle 123456
Vehicle 123456 is entering state "ready"
Vehicle 123456 is entering state "battery_low"
Vehicle 123456 is entering state "bounty"
Sms sent: Vehicle availalbe to collect. Come get me at {Longitude:59.332336 Latitude:18.06416}
Vehicle 123456 is entering state "collected"
Hunter 1000 collected vehicle 123456
Vehicle 123456 is entering state "dropped"
Hunter 1000 dropped vehicle 123456 at office
Vehicle 123456 is entering state "ready"
Hunter 1000 delivered vehicle 123456 to {12.345 56.789}
Admin 2000 sent vehicle 123456 to service mode
Admin 2000 terminated vehicle 123456
```