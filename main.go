package main

import (
	"time"

	"./v_user"
	"./v_vehicle"
)

func main() {

	v := v_vehicle.New(
		123456,
		&v_vehicle.Coordinates{Longitude: 59.332335, Latitude: 18.064159},
		&v_user.UserPriviledges{
			v_user.EndUser: []string{
				v_vehicle.EventStartJourney,
				v_vehicle.EventEndJourney,
			},
			v_user.Hunter: []string{
				v_vehicle.EventCollect,
				v_vehicle.EventDrop,
				v_vehicle.EventDeliver,
			},
		},
	)

	someUser := &v_user.User{UserID: 999, UserLevel: v_user.EndUser}
	hunter := &v_user.User{UserID: 1000, UserLevel: v_user.Hunter}
	admin := &v_user.User{UserID: 2000, UserLevel: v_user.Admin}

	v.StartJourney(someUser)
	v.EndJourney(someUser)
	v.LowBattery()

	timer := time.NewTimer(time.Second)
	<-timer.C
	v.Collect(hunter)
	v.Drop(hunter)
	v.Deliver(hunter, &v_vehicle.Coordinates{Longitude: 12.345, Latitude: 56.789})
	v.ServiceMode(admin)
	v.Terminate(admin)

	for {
	}
}
