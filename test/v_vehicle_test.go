package test

import (
	"testing"
	"time"

	"../v_user"
	"../v_vehicle"
)

func TestCompleteJourney(t *testing.T) {

	// Given
	v := createVehicle()

	someUser := &v_user.User{UserID: 999, UserLevel: v_user.EndUser}
	hunter1 := &v_user.User{UserID: 1000, UserLevel: v_user.Hunter}
	hunter2 := &v_user.User{UserID: 1001, UserLevel: v_user.Hunter}

	// When
	v.StartJourney(someUser)
	// Then
	if v.Current() != v_vehicle.StateRiding {
		t.Errorf("State was incorrect, got: %v, want: %v.", v.Current(), v_vehicle.StateRiding)
	}

	// When
	v.EndJourney(someUser)
	// Then
	if v.Current() != v_vehicle.StateReady {
		t.Errorf("State was incorrect, got: %v, want: %v.", v.Current(), v_vehicle.StateReady)
	}

	// When
	v.LowBattery()
	// Then
	if v.Current() != v_vehicle.StateBatteryLow {
		t.Errorf("State was incorrect, got: %v, want: %v.", v.Current(), v_vehicle.StateBatteryLow)
	}

	// When
	err := v.StartJourney(someUser)
	// Then
	if err == nil {
		t.Errorf("StartJourney result was incorrect, got: nil, want: error.")
	}

	// When
	timer := time.NewTimer(time.Second)
	<-timer.C
	err = v.Collect(someUser)
	// Then
	if err == nil {
		t.Errorf("Collect result was incorrect, got: nil, want: error.")
	}

	// When
	err = v.Collect(hunter1)
	// Then
	if err != nil {
		t.Errorf("Collect result was incorrect, got: %v, want: nil.", err)
	}

	// When
	err = v.Drop(someUser)
	// Then
	if err == nil {
		t.Errorf("Drop result was incorrect, got: nil, want: error.")
	}

	// When
	err = v.Drop(hunter2)
	// Then
	if err == nil {
		t.Errorf("Drop result was incorrect, got: %v, want: nil.", err)
	}

	// When
	err = v.Drop(hunter1)
	// Then
	if err != nil {
		t.Errorf("Drop result was incorrect, got: %v, want: nil.", err)
	}

	// When
	err = v.Deliver(hunter2, &v_vehicle.Coordinates{Longitude: 12.345, Latitude: 56.789})
	// Then
	if err != nil {
		t.Errorf("Deliver result was incorrect, got: %v, want: nil.", err)
	}

	// When
	v.LowBattery()
	timer.Reset(time.Second)
	<-timer.C
	// Then
	if v.Current() != v_vehicle.StateBounty {
		t.Errorf("State was incorrect, got: %v, want: %v.", v.Current(), v_vehicle.StateBounty)
	}
}

func TestUnknownState(t *testing.T) {
	// Given
	v := createVehicle()

	// When
	timer := time.NewTimer(time.Second * 3)
	<-timer.C
	// Then
	if v.Current() != v_vehicle.StateUnknown {
		t.Errorf("State was incorrect, got: %v, want: %v.", v.Current(), v_vehicle.StateUnknown)
	}

	// When
	someUser := &v_user.User{UserID: 999, UserLevel: v_user.EndUser}
	err := v.StartJourney(someUser)
	// Then
	if err == nil {
		t.Errorf("StartJourney result was incorrect, got: nil, want: error.")
	}

}

func TestServiceModeState(t *testing.T) {
	// Given
	v := createVehicle()
	someUser := &v_user.User{UserID: 999, UserLevel: v_user.EndUser}
	admin := &v_user.User{UserID: 1000, UserLevel: v_user.Admin}

	// When
	err := v.ServiceMode(someUser)
	// Then
	if err == nil {
		t.Errorf("State was incorrect, got: nil, want: error.")
	}

	// When
	v.ServiceMode(admin)
	// Then
	if v.Current() != v_vehicle.StateServiceMode {
		t.Errorf("State was incorrect, got: %v, want: %v.", v.Current(), v_vehicle.StateServiceMode)
	}

	// When
	err = v.StartJourney(someUser)
	// Then
	if err == nil {
		t.Errorf("StartJourney result was incorrect, got: nil, want: error.")
	}

}

func createVehicle() *v_vehicle.Vehicle {
	userPriviledge := v_user.UserPriviledges{
		v_user.EndUser: []string{
			v_vehicle.EventStartJourney,
			v_vehicle.EventEndJourney,
		},
		v_user.Hunter: []string{
			v_vehicle.EventCollect,
			v_vehicle.EventDrop,
			v_vehicle.EventDeliver,
		},
	}

	return v_vehicle.New(
		123456,
		&v_vehicle.Coordinates{Longitude: 59.332335, Latitude: 18.064159},
		&userPriviledge,
	)
}
