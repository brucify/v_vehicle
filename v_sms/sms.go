package sms

import (
	"fmt"
)

//Send a new sms, returns true if sms is sent
func Send(text string) bool {
	fmt.Println("Sms sent: " + text)
	return true
}
