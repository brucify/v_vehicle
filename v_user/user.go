package v_user

// User of the vehicle
type User struct {
	UserID    int
	UserLevel string
}

// UserPriviledges one user level is mapped to multiple allowed events
type UserPriviledges map[string][]string

const EndUser = "end_user"
const Hunter = "hunter"
const Admin = "admin"

// IsAllowed return true if the user is allowed to access this event
func (u *User) IsAllowed(event string, priviledges *UserPriviledges) bool {
	if allowedEvents := (*priviledges)[u.UserLevel]; allowedEvents != nil {
		for _, e := range allowedEvents {
			if e == event {
				return true
			}
		}
	}

	return false
}
