package v_vehicle

import (
	"errors"
	"fmt"

	"time"

	"github.com/looplab/fsm"

	"../v_sms"
	"../v_user"
)

// Vehicle represents a vehicle
type Vehicle struct {
	fsm       *fsm.FSM
	vehicleID int
	timer     *time.Timer
	ticker    *time.Ticker

	userPriviledges *v_user.UserPriviledges
	Coordinates     *Coordinates
	BatteryLevel    float32
	CurrentRiderID  int
	CurrentHunterID int
}

// Coordinates of the vehicle
type Coordinates struct {
	Longitude float32
	Latitude  float32
}

const StateReady = "ready"
const StateRiding = "riding"
const StateBatteryLow = "battery_low"
const StateBounty = "bounty"
const StateCollected = "collected"
const StateDropped = "dropped"
const StateUnknown = "unknown"
const StateServiceMode = "service_mode"
const StateTerminated = "terminated"

//EndUser events
const EventStartJourney = "start_journey"
const EventEndJourney = "end_journey"

//Hunter events
const EventCollect = "collect"
const EventDrop = "drop"
const EventDeliver = "deliver"

//Other events
const EventBatteryLow = "battery_is_low"
const EventAfter2130 = "after_2130"
const EventSmsSent = "sms_sent"
const EventIdleFor48H = "idle_for_48_hours"

const defaultIdleTime = time.Second * 2
const defaultTickerInterval = time.Second * 1

// New returns a new vehicle
func New(vehicleID int, coordinates *Coordinates, userPriviledges *v_user.UserPriviledges) *Vehicle {
	v := &Vehicle{
		BatteryLevel:    100.0,
		Coordinates:     coordinates,
		vehicleID:       vehicleID,
		userPriviledges: userPriviledges,
		timer:           time.NewTimer(defaultIdleTime),
		fsm: fsm.NewFSM(
			StateReady,
			fsm.Events{
				//ready state
				{Name: EventDeliver, Src: []string{StateDropped}, Dst: StateReady},
				{Name: EventEndJourney, Src: []string{StateRiding}, Dst: StateReady},

				//riding state
				{Name: EventStartJourney, Src: []string{StateReady}, Dst: StateRiding},

				//battery_low state
				{Name: EventBatteryLow, Src: []string{StateRiding, StateReady}, Dst: StateBatteryLow},

				//bounty state
				{Name: EventAfter2130, Src: []string{StateReady}, Dst: StateBounty},
				{Name: EventSmsSent, Src: []string{StateBatteryLow}, Dst: StateBounty},

				//collected state
				{Name: EventCollect, Src: []string{StateBounty}, Dst: StateCollected},

				//dropped state
				{Name: EventDrop, Src: []string{StateCollected}, Dst: StateDropped},

				//unknown state
				{Name: EventIdleFor48H, Src: []string{StateReady}, Dst: StateUnknown},
			},
			fsm.Callbacks{
				"enter_state": func(e *fsm.Event) {
					fmt.Printf("Vehicle %d is entering state \"%v\"\n", vehicleID, e.Dst)
				},
			},
		),
	}

	v.startTimer()
	return v
}

//Current returns the vehicle's current state
func (v *Vehicle) Current() string {
	return v.fsm.Current()
}

//AvailableTransitions returns the vehicle's next available state
func (v *Vehicle) AvailableTransitions() []string {
	return v.fsm.AvailableTransitions()
}

//Visualize returns the vehicle's states
func (v *Vehicle) Visualize() string {
	return fsm.Visualize(v.fsm)
}

//StartJourney start riding the vehicle
func (v *Vehicle) StartJourney(u *v_user.User) error {
	err := v.event(EventStartJourney)
	if err == nil {
		v.CurrentRiderID = u.UserID
		v.stopTimerAndTicker()
		fmt.Printf("User %d started journey on vehicle %d\n", v.CurrentRiderID, v.vehicleID)
	}

	return err
}

//EndJourney finish riding the vehicle
func (v *Vehicle) EndJourney(u *v_user.User) error {
	if v.CurrentRiderID == u.UserID {
		fmt.Printf("User %d ended journey on vehicle %d\n", v.CurrentRiderID, v.vehicleID)
		err := v.event(EventEndJourney)
		if err == nil {
			v.CurrentRiderID = 0
		}

		v.startTimer()

		return err
	}

	return errors.New("Forbidden")
}

//Collect a hunter initiates the collection. Will trigger EventCollect
func (v *Vehicle) Collect(u *v_user.User) error {
	if u.IsAllowed(EventCollect, v.userPriviledges) {
		err := v.event(EventCollect)
		if err == nil {
			v.CurrentHunterID = u.UserID
			fmt.Printf("Hunter %d collected vehicle %d\n", v.CurrentHunterID, v.vehicleID)
		}
		return err
	}

	return errors.New("Forbidden")
}

//Drop a hunter initiates the drop. Will trigger EventDrop. Must be the same hunter who collected
func (v *Vehicle) Drop(u *v_user.User) error {
	if u.IsAllowed(EventDrop, v.userPriviledges) {
		if v.CurrentHunterID == u.UserID {
			err := v.event(EventDrop)
			if err == nil {
				fmt.Printf("Hunter %d dropped vehicle %d at office\n", v.CurrentHunterID, v.vehicleID)
				v.CurrentHunterID = 0
			}
			return err
		}
	}

	return errors.New("Forbidden")
}

//Deliver a hunter has finished the delivery. Will trigger EventDeliver
func (v *Vehicle) Deliver(u *v_user.User, coordinates *Coordinates) error {
	if u.IsAllowed(EventDeliver, v.userPriviledges) {
		err := v.event(EventDeliver)
		if err == nil {
			v.Coordinates = coordinates
			fmt.Printf("Hunter %d delivered vehicle %d to %v\n", u.UserID, v.vehicleID, *v.Coordinates)
		}

		v.startTimer()
		return err
	}

	return errors.New("Forbidden")
}

//LowBattery notifies the vehicle about low battery
func (v *Vehicle) LowBattery() error {
	err := v.event(EventBatteryLow)
	if err == nil {
		go notifyHunters(v)
		v.stopTimerAndTicker()
	}

	return err
}

//ServiceMode admin sets the state to StateServiceMode
func (v *Vehicle) ServiceMode(u *v_user.User) error {
	if u.UserLevel == v_user.Admin {
		v.fsm.SetState(StateServiceMode)
		v.stopTimerAndTicker()
		fmt.Printf("Admin %d sent vehicle %d to service mode\n", u.UserID, v.vehicleID)
		return nil
	}

	return errors.New("Forbidden")
}

//Terminate admin sets the state to StateTerminated
func (v *Vehicle) Terminate(u *v_user.User) error {
	if u.UserLevel == v_user.Admin {
		v.fsm.SetState(StateTerminated)
		v.stopTimerAndTicker()
		fmt.Printf("Admin %d terminated vehicle %d\n", u.UserID, v.vehicleID)
		return nil
	}

	return errors.New("Forbidden")
}

//NotifyHunters sends sms to hunters
func notifyHunters(v *Vehicle) {
	go sms.Send(fmt.Sprintf("Vehicle availalbe to collect. Come get me at %+v", *v.Coordinates))
	v.event(EventSmsSent)
}

func (v *Vehicle) event(event string) error {
	err := v.fsm.Event(event)
	if err != nil {
		fmt.Println(err)
	}

	return err
}

func (v *Vehicle) startTimer() {
	v.timer.Reset(defaultIdleTime)
	go func() {
		<-v.timer.C
		fmt.Printf("Time is up for vehicle %d\n", v.vehicleID)
		v.event(EventIdleFor48H)
	}()

	v.ticker = time.NewTicker(defaultTickerInterval)
	go func() {
		for range v.ticker.C {
			nineThirtyToday := time.Date(
				time.Now().Year(),
				time.Now().Month(),
				time.Now().Day(), 21, 30, 0, 0, time.Now().Location())
			sixThirtyToday := time.Date(
				time.Now().Year(),
				time.Now().Month(),
				time.Now().Day(), 6, 30, 0, 0, time.Now().Location())
			if time.Now().After(nineThirtyToday) || time.Now().Before(sixThirtyToday) {
				fmt.Printf("It's between 2130 and 0630 local time for vehicle %d\n", v.vehicleID)
				v.stopTimerAndTicker()
				v.event(EventAfter2130)
			}
		}
	}()
}

func (v *Vehicle) stopTimerAndTicker() bool {
	v.ticker.Stop()
	return v.timer.Stop()
}
